import cv2
import numpy as np
from picamera import PiCamera
import time
def nothing(x):
    pass


def prise_de_la_photo():
    camera = PiCamera()
    cv2.destroyAllWindows()
    video_capture = cv2.VideoCapture(0)
    font = cv2.FONT_HERSHEY_SIMPLEX

    # Create a black image, a window
    frame = np.zeros((300,300,3), np.uint8)
    cv2.namedWindow('image')

    # create switch for ON/OFF functionality
    switch = 'Curseur'
    cv2.createTrackbar(switch, 'image',0,1,nothing)
    #video_capture = cv2.VideoCapture()
    #numerodephoto=1
    indi=0
    while(1):

        cv2.imshow('image',frame)
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break
        s = cv2.getTrackbarPos(switch,'image')

        if s == 0:
            video_capture.release
            frame[:] = 0
            cv2.putText(frame,'Balayer le curseur pour',(120,200), font, 1,(255,255,255),2,cv2.LINE_AA)
            cv2.putText(frame,'prendre une photo',(167,240), font, 1,(255,255,255),2,cv2.LINE_AA)
        else:
            #ret, frame = video_capture.read()
            #frame = cv2.flip(frame, 1)
            
            if indi==0:
                camera.start_preview()
                time.sleep(2)
                debutswitch= time.time()
                indi=1
            temps=time.time()
            duree=temps-debutswitch
            if duree<=1:
                #cv2.putText(frame,'Photo dans 3s',(10,50), font, 1,(255,255,255),2,cv2.LINE_AA)
                camera.annotate_text = "Photo dans 3s"
            elif duree <= 2:
                #cv2.putText(frame,'Photo dans 2s',(10,50), font, 1,(255,255,255),2,cv2.LINE_AA)
                camera.annotate_text = "Photo dans 2s"
            elif duree <=3:
                #cv2.putText(frame,'Photo dans 1s',(10,50), font, 1,(255,255,255),2,cv2.LINE_AA)
                camera.annotate_text = "Photo dans 1s"
            elif duree>3:
                #cv2.putText(frame,'    ',(100,200), font, 4,(255,255,255))
                #cv2.imwrite('capture1.png',frame)
                """
                numerodephoto+=1
                indi=0
                cv2.setTrackbarPos(switch, 'image',0)
                """
                camera.annotate_text = " "
                camera.capture('capture1.jpg')
                camera.stop_preview()
                cv2.destroyAllWindows()
                break

