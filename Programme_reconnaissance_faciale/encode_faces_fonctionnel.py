

### Encodage des Images de la base de données ###


from imutils import paths
import face_recognition #précision de 99.38%
import argparse
import pickle
import cv2
import os


#initialisation du modele de detection soit hog soit cnn
#cnn est plus efficace mais plus long
#hog est plus rapide mais moins efficace
modele_de_detection="hog"


def encodage_de_base_de_donnees(data_set,encodage):

    # Saisie des chemins vers les images d'entrée dans notre ensemble de données
    print("[INFO] quantifying faces...")
    imagePaths = list(paths.list_images(data_set))

    # Initialisation de la liste des encodages de visage connus et des noms correspondant pour chaque personne
    knownEncodings = []
    knownNames = []

    for (i, imagePath) in enumerate(imagePaths): #Boucle sur le chemin de chaque image
        print("[INFO] processing image {}/{}".format(i + 1,
            len(imagePaths)))
        name = imagePath.split(os.path.sep)[-2] # Extraction du nom de la personne à partir du chemin de l'image

        image = cv2.imread(imagePath) #Chargement de l'image et conversion vers cv2.imread
        rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) # Echange de la convention couleur (passage de BGR à RGB pour dlib)

        # Détection et localisation du visage pour chaque itération de la boucle
        boxes = face_recognition.face_locations(rgb,
            model=modele_de_detection) #Permet de trouver les coordonnées (x,y) du cadre et fait correspondre chaque visage à l'image d'entrée

        encodings = face_recognition.face_encodings(rgb, boxes) # Vectorisation du visage à l'aide de la méthode face_recognition.face_encodings
    # Sur dlib, le vecteur en sortie est une liste de 128 nombres réels, utilisé pour quantifier un visage
        for encoding in encodings: #Boucle sur les encodages afin d'ajouter chaque encodage et chaque nom à la liste appropriée
            knownEncodings.append(encoding)
            knownNames.append(name)

    print("[INFO] serializing encodings...")
    data = {"encodings": knownEncodings, "names": knownNames} #Construction d'un dictionnaire avec deux clés
    f = open(encodage, "wb")
    f.write(pickle.dumps(data))
    f.close() # envoyer les noms et encodages des visages vers le disque pour un éventuel rappel ultérieur
