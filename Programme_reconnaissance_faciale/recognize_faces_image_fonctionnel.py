
#importation des packages
import face_recognition
import argparse
import pickle
import cv2
import time
bd=[["Antonio_Loison","Absent"],["Wandrille_Leboucher","Absent"],["Theo_Dupuis","Absent"],["Hassan_Raji","Absent"],["Alexandre_Derreumaux","Absent"]]
import datetime
#initialisation du modele de detection soit hog soit cnn
#cnn est plus efficace mais plus long
#hoog est plus rapide mais moins efficace
modele_de_detection="hog"

def reconnaissance_faciale(encodage,image_a_traiter):


    #Chargement des visages connus et des encodages pré-calculés
    print("[INFO] loading encodings...")
    data = pickle.loads(open(encodage, "rb").read())

    #Chargement de l'image d'entrée
    image = cv2.imread(image_a_traiter)
    rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) # Conversion de BGR vers RGB

    #Détection de tous les visages dans l'image d'entrée
    print("[INFO] recognizing faces...")
    boxes = face_recognition.face_locations(rgb,
        model=modele_de_detection)
    encodings = face_recognition.face_encodings(rgb, boxes)

    # Initialisation d'une liste de noms pour chaque visage détecté
    names = []


    for encoding in encodings:
        matches = face_recognition.compare_faces(data["encodings"],
            encoding) # On tente de faire correspondre chaque visage dans l'image d'entrée à nos visages connus dans la base de données
        name = "Unknown"

    # La fonction compare_faces calcule la distance euclidienne entre le visage détecté et tous les visages de la base de données. Si la distance est suffisamment petite, on retourne True.

        if True in matches:
            # On cherche le nombre de correspondance entre les visages
            matchedIdxs = [i for (i, b) in enumerate(matches) if b]
            # On initialise un dictionnaire pour compter le nombre total de fois où chaque visage a matché avec un visage de la base de données.
            counts = {}

            for i in matchedIdxs:
                name = data["names"][i]
                counts[name] = counts.get(name, 0) + 1
            print(counts)

            if max(counts.values()) < 5 : #Limite de points en dessous de laquelle on renvoie "Unknown"
                name = "Unknown"
            else :
                name = max(counts, key=counts.get)
            # On determine le visage reconnu ayant le plus grand nombre de correspondances"""
            """name = max(counts, key=counts.get)"""

        # Mise à jour de la liste
        names.append(name)
        print(names) # On affiche le nom des personnes reconnues


    for ((top, right, bottom, left), name) in zip(boxes, names):
        # On écrit le nom et prénom de la personne à coté de son visage reconnu
        cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0), 2)
        y = top - 15 if top - 15 > 15 else top + 15
        cv2.putText(image, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
            0.75, (0, 255, 0), 2)

    cv2.imshow("Image", image)
    cv2.waitKey(0)


    def marquer_present(base_de_donnée, eleves_presents):
        #modifie la base de donnée  en fonction des presences
        nbeleves=len(base_de_donnée)
        for elevephoto in eleves_presents:
            for n in range(nbeleves):
                if base_de_donnée[n][0]==elevephoto:
                    base_de_donnée[n][1]="Present"
        return base_de_donnée


    def EXCELfinal(base_de_donnée):
        #genere un bloc note avec les presences
        fichier = open('EXCEL.txt','a')
        maintenant= datetime.datetime.now()
        #date de l'appel
        date=str(maintenant.day)+'/'+str(maintenant.month)+'/'+str(maintenant.year)
        fichier.write("\n\n"+date)
        for eleves in base_de_donnée:
            fichier.write( "\n"+str(eleves[0])+"   "+str(eleves[1]))

    EXCELfinal(marquer_present(bd,names))
#reconnaissance_faciale("encoding_unif.pickle",r"C:\Users\loiso\26_appelvideo\Programme_reconnaissance_faciale\Data_test\john_cagnol.jpg")
